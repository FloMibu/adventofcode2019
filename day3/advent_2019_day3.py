import numpy as np
import csv

def move_path( elt_in, way):
    list_out = list()
    direction = way[0]
    steps = way[1:]
    move_dir_str = direction.replace('R', '[0] ').replace('L', '[0] ').replace('U', '[1] ').replace('D', '[1] ')
    move_val_str = direction.replace('R', '+1').replace('L', '-1').replace('U', '+1').replace('D','-1')
    elt_out = [ ee for ee in elt_in]
    for ii in range( int(steps)):
        elt_out = [ ee for ee in elt_out]
        exec( 'elt_out' + move_dir_str + '= elt_out' +  move_dir_str + ' +' + move_val_str)
        elt_out[2] = elt_out[2]+1
        list_out.append( elt_out)
    return list_out

def compute_path( list_way):
    list_path = list()
    list_path.append( [0,0,0])
    for way in list_way:
        for elt in move_path(list_path[-1], way):
            list_path.append( elt)
    return list_path

#def get_intersection( lst1, lst2):
#    return [value for value in lst1 if value in lst2]

# main 
with open('input_2019_3', 'rb') as f:
    reader = csv.reader(f, delimiter=',')
    list_ways = list(reader)

list_path = list()
for elt in list_ways:
    list_path.append( compute_path( elt)) 

#list_intersect = [value for value in list_path[0][1:] if value in list_path[1][1:]]
coords_2 = [ [la,lo] for [la,lo,e] in list_path[1][1:]  ]
list_intersect = [ [lat, lon, step1+ list_path[1][ 1+coords_2.index([lat, lon])][2]] for [lat, lon, step1] in list_path[0][1:] if [lat, lon] in coords_2 ]

distance = [ d1+d2 for [d1,d2,steps] in list_intersect]
steps = [ step for [d1,d2,step] in list_intersect]

print "Day 3 : the minimum distance is ", min(distance)
print "Day 3 : but the minimum steps ", min(steps)

