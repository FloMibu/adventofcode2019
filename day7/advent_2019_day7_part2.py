import numpy as np
import copy

#############################
# day 5 - 1

prog = np.genfromtxt('input_2019_7', dtype='int', delimiter=',').tolist()

def decode_upcode( digit):
    stry = '0000' + str(digit)
    opcode = int(stry[-2:])
    error=False
    list_out = list()
    for elt in stry[0:-2]:
        if elt in ['0','1']:
            list_out.append(bool(int(elt)))
        else:
            list_out.append(elt)
            error = True
    list_out.reverse()
    return opcode, list_out, error

def opcoding( alarm, input0, input1):
  input_in = input0
  index = 0
  wrong = False
  output = 8888 # np.nan
  ii=-1
  firstinput=0
  while alarm[index] <> 99:
    pointerModified = False # by default, exception for opcode 5
    pointerValue = 0
    ii +=1
    #print alarm
    opcode,list_op, error = decode_upcode( alarm[index])
    if error:
        print 'Error with: ', alarm[index], index
        break
    if opcode == 1:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        alarm[ ind2] = alarm[ind0] + alarm[ind1]
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
    elif opcode == 2:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        alarm[ ind2] = alarm[ind0] * alarm[ind1]
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
    elif opcode == 3:
        if list_op[0]:
            ind2 = index+1
        else:
            ind2 = alarm[index+1]
        if firstinput==0:
            alarm[ ind2] = input0
        elif firstinput==1:
            alarm[ ind2] = input1 # UGLY
        elif firstinput>1:
            alarm[ ind2] = input_in # UGLY
        firstinput +=1
        #alarm[ ind2] = input("I need input:")
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
    elif opcode == 4:
        if list_op[0]:
            output = alarm[ index+1]
        else:
            output = alarm[ alarm[index+1]]
        print 'Output: ', output
        input_in = output
    elif opcode == 5:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] <>0:
            alarm[ index] = alarm[ ind1]
            pointerModified = True
            pointerValue = alarm[ ind1]
        else:
            pointerModified = False
    elif opcode == 6:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] ==0:
            alarm[ index] = alarm[ ind1]
            pointerModified = True
            pointerValue = alarm[ ind1]
    elif opcode == 7:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] < alarm[ ind1]:
            alarm[ ind2] = 1
        else:
            alarm[ ind2] = 0
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[ index]
    elif opcode == 8:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] == alarm[ ind1]:
            alarm[ ind2] = 1
        else:
            alarm[ ind2] = 0
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[ index]
    elif opcode == 99:
      return output, alarm
    elif opcode <> 99:
    #    wrong = True
        break
    index_sav = index*1
    if opcode in [1,2,99,7,8]:
        index = index+4
    elif opcode in [5,6]:
        index = index+3
    elif opcode in [3,4]:
        index = index+2
    if pointerModified:
        index = pointerValue
  if wrong:
      return np.nan
  else:
      return output, alarm

test = copy.deepcopy( prog)

# test everything
list_phases = [[0, 1, 2, 3, 4], [0, 1, 2, 4, 3]] 

jj = 2
for elt in copy.deepcopy(list_phases):
  for ii in [4, 3]:
      ind1 = elt.index(jj)
      ind2 = elt.index(ii)
      elt_in = copy.deepcopy(elt)
      elt_in[ ind1] = ii
      elt_in[ ind2] = jj
      list_phases.append( elt_in)
jj = 1
for elt in copy.deepcopy(list_phases):
  for ii in [4, 3, 2]:
      ind1 = elt.index(jj)
      ind2 = elt.index(ii)
      elt_in = copy.deepcopy(elt)
      elt_in[ ind1] = ii
      elt_in[ ind2] = jj
      list_phases.append( elt_in)
jj = 0
for elt in copy.deepcopy(list_phases):
  for ii in [4, 3, 2, 1]:
      ind1 = elt.index(jj)
      ind2 = elt.index(ii)
      elt_in = copy.deepcopy(elt)
      elt_in[ ind1] = ii
      elt_in[ ind2] = jj
      list_phases.append( elt_in)

max_thrusters = 0
for elt in list_phases:
    input_in = 0
    for phases in elt:
        prog_in = copy.deepcopy(prog)
        output, prog_set = opcoding( prog_in, phases, input_in) 
        print "phase: ", output
        #print "True prog phase: ", phases
        #output, toto = opcoding( prog_set, input_in)
        input_in = output*1
    max_thrusters = max( max_thrusters, output)
    print max_thrusters, output


