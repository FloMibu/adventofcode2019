
from operator import add, mul, ne, gt, lt, eq
import numpy as np
from copy import deepcopy 
import matplotlib.pyplot as plt

day = 11
do_tests = True
increase_memory = 10000

opcode_ins = { 1: {'oper': add, 'len_code': 3, 'typ_val': 'oper'},
               2: {'oper': mul, 'len_code': 3, 'typ_val': 'oper'},
               3: {             'len_code': 1, 'typ_val': 'Input'},
               4: {             'len_code': 1, 'typ_val': 'Output'},
               5: {'oper': ne,  'len_code': 2, 'typ_val': 'cond_noelse0'},
               6: {'oper': eq,  'len_code': 2, 'typ_val': 'cond_noelse0'},
               7: {'oper': lt,  'len_code': 3, 'typ_val': 'cond_else', 'val': [0,1]},
               8: {'oper': eq,  'len_code': 3, 'typ_val': 'cond_else', 'val': [0,1]},
               9: {'oper': '',  'len_code': 1, 'typ_val': '' },
               99: {'len_code': 0,'typ_val': ''}}

class class_program:
    prog = list()
    index = np.nan
    relat = np.nan
    state = np.nan
    registre = np.nan

def set_registre(self, inputin):
    self.registre = inputin
    return self

def init_prog( prog, inputy=0):
    progy = class_program()
    progy.prog = prog
    # add memory - should be dynamic
    for ii in range(increase_memory):
      progy.prog = progy.prog + [np.nan]
    progy.index = 0
    progy.relat = 0
    progy.registre = inputy
    return progy

def set_param_indexes(mod, i, program):
    if mod == 0:
        param = program.prog[ program.index+i]
    elif mod == 1:
        param = program.index+i
    elif mod == 2:
        param = program.relat + program.prog[ program.index+i]
    return param 

def decode_opcode(program):
    ins = program.prog[ program.index]
    opcode = ins % 100
    modes = [ int(ii) for ii in ("%05d" % ins)[0:-2] ]
    parameters = [set_param_indexes( modes[-ii-1], ii+1, program) for ii in range( opcode_ins[opcode]['len_code'])]
    return opcode, parameters

def intcode_machine( program):
    opcode = -1
    while opcode < 99 and opcode <>4:
        opcode, parameters = decode_opcode(program) 
        #print opcode
        # parameters indices
        # c is always the index where to assign a value
        if opcode_ins[opcode]['len_code'] == 1:
            c =       [ parameters[i] for i in range(1)][0]
        elif opcode_ins[opcode]['len_code'] == 2:
            a, c =    [ parameters[i] for i in range(2)]
        elif opcode_ins[opcode]['len_code'] == 3:
            a, b, c = [ parameters[i] for i in range(3)]
        # compute the operation
        if opcode_ins[opcode]['len_code'] == 3: # 1 2 8 7 
            operation = opcode_ins[opcode]['oper']( program.prog[ a], program.prog[ b])
        if opcode_ins[opcode]['typ_val'] == 'cond_noelse0':
            operation = opcode_ins[opcode]['oper']( program.prog[ a], 0)
        # actuel job
        if opcode_ins[opcode]['typ_val'] == 'oper': # case 1, 2
            program.prog[ c] = operation
        if opcode_ins[opcode]['typ_val'] == 'Input': # case 3
            program.prog[ c] = 1* program.registre
            #program.registre = np.nan # we empty the registre. we assume it is red only once
        if opcode_ins[opcode]['typ_val'] == 'Output': # case 4
            program.registre = 1* program.prog[ c]
        if 'cond_else' == opcode_ins[opcode]['typ_val']: # case 7, 8
            program.prog[ c] = opcode_ins[opcode]['val'][ operation]
        if 'cond_noelse0' == opcode_ins[opcode]['typ_val']: # case 5 6
            if operation: 
                jump = - ( program.index + opcode_ins[opcode]['len_code'] + 1) + program.prog[ c]
            else:
                jump = 0
        else: # case not 5 6
            jump = 0
        if opcode == 9:
            program.relat = program.relat + program.prog[ c]
        program.index = program.index + opcode_ins[opcode]['len_code'] + 1 + jump
        program.state = opcode
        if program.state == 4:
            break
        if program.state == 99:
            break
        #    program.registre = np.nan
    return program

def amplifiers( prog, input_in, amp_in, phase_settings, amp_out, IO_table): 
    # init amplifiers
    for ii in range(len(phase_settings)):
        amp = init_prog( prog, phase_settings[ii])
        while amp.state<>99:
            amp = intcode_machine( deepcopy(amp))
        print 'phase', phase_settings[ii], amp.prog
        if ii == amp_in:
            amp.registre = input_in
        else:
            amp.registre = np.nan
        #amp.index = 0
        exec("amp_"+str(ii)+" = deepcopy(amp)")
    # do the loop, if any
    no_more_outputs = False
    while not no_more_outputs:
        no_more_outputs = True
        for ii in range(len(phase_settings)):
            exec("amp_temp = deepcopy(amp_"+str(ii)+")")
            print 'Doing amp', ii, amp_temp.registre
            if not np.isnan( amp_temp.registre):
                amp_temp = intcode_machine( amp_temp)
                print amp_temp.registre
                if not np.isnan( amp_temp.registre):
                    # send the output to the appropriate amp
                    if ii in IO_table:
                        no_more_outputs = False
                        exec("amp_"+str( IO_table[ii])+".registre = deepcopy(amp_temp.registre)")
                    # main output
                    if ii == amp_out:
                        main_output = amp_temp.registre
                        print main_output
                    # empty registre: signal only send once
                    amp_temp.registre = np.nan
            exec("amp_"+str(ii)+" = deepcopy(amp_temp)")
    return main_output

class class_robot():
    way = 0 #0 90 180 270..etc
    coords = (0,0)

def update_coords( coords, way):
   #x, y = coords.split(';')
   #x, y = float(x), float(y)
   #y = y + np.around( np.cos( float(way ) /180*np.pi), 1)
   #x = x + np.around( np.sin( float(way ) /180*np.pi), 1)
   coords_out = (  coords[0] + np.around( np.sin( float(way ) /180*np.pi), 0), coords[1] + np.around( np.cos( float(way ) /180*np.pi), 0))
   #np.around( np.sin( float(way ) /180*np.pi), 0), + np.around( np.cos( float(way ) /180*np.pi), 0)
   #coords[0] = coords[0] + np.around( np.sin( float(way ) /180*np.pi), 1)
   #coords[1] = coords[1] + + np.around( np.cos( float(way ) /180*np.pi), 1)
   #y = y + np.around( np.cos( float(way ) /180*np.pi), 1)
   #x = x + np.around( np.sin( float(way ) /180*np.pi), 1)
   #return str(x) + ';' + str(y)
   return coords_out

##########################################
###  Days computation
##########################################

if day == 2:
    print "Day is:", day
    print "test1: ", intcode_machine( init_prog( [1,0,0,0,99])).prog == [2,0,0,0,99] 
    print "test2: ", intcode_machine( init_prog( [2,3,0,3,99])).prog == [2,3,0,6,99] 
    print "test3: ", intcode_machine( init_prog( [2,4,4,5,99,0])).prog == [2,4,4,5,99,9801] 
    print "test4: ", intcode_machine( init_prog( [1,1,1,4,99,5,6,0,99])).prog == [30,1,1,4,2,5,6,0,99] 
    # part 1
    alarm = np.genfromtxt('input_2019_2', dtype='int', delimiter=',').tolist()
    alarm[1] = 12; alarm[2] = 2
    print 'Results 1 :', intcode_machine( init_prog( alarm)).prog[0]

if day == 5:
    print "Day is:", day
    if do_tests:
        print "test1: ", intcode_machine( init_prog( [3,0,4,0,99], 12345)).registre == 12345
        print "test2: ", intcode_machine( init_prog( [1002,4,3,4,33])).prog[4] == 99
        print "test3: ", intcode_machine( init_prog( [1101,100,-1,4,0])).prog[4] == 99
    # part 1
    puzzle = np.genfromtxt('input_2019_' + str(day), dtype='int', delimiter=',').tolist()
    prog_in = init_prog( puzzle, 1)
    while prog_in.state<>99:
        prog_in = intcode_machine( deepcopy(prog_in))
    print "Result 1: ", prog_in.registre
    #test 2
    if do_tests:
        print "test11: ", intcode_machine( init_prog( [3,9,8,9,10,9,4,9,99,-1,8], 8)).registre == 1
        print "test12: ", intcode_machine( init_prog( [3,9,8,9,10,9,4,9,99,-1,8], 7)).registre == 0
        print "test21: ", intcode_machine( init_prog( [3,9,7,9,10,9,4,9,99,-1,8], 7)).registre == 1
        print "test22: ", intcode_machine( init_prog( [3,9,7,9,10,9,4,9,99,-1,8], 9)).registre == 0
        print "test31: ", intcode_machine( init_prog( [3,3,1108,-1,8,3,4,3,99], 8)).registre == 1
        print "test32: ", intcode_machine( init_prog( [3,3,1108,-1,8,3,4,3,99], 9)).registre == 0
        print "test41: ", intcode_machine( init_prog( [3,3,1107,-1,8,3,4,3,99], 7)).registre == 1
        print "test42: ", intcode_machine( init_prog( [3,3,1107,-1,8,3,4,3,99], 9)).registre == 0
        print "test51: ", intcode_machine( init_prog( [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0)).registre == 0
        print "test52: ", intcode_machine( init_prog( [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 2)).registre == 1
        print "test61: ", intcode_machine( init_prog( [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 0)).registre == 0
        print "test62: ", intcode_machine( init_prog( [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 2)).registre == 1
        print "test71: ", intcode_machine( init_prog( [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 7)).registre == 999
        print "test72: ", intcode_machine( init_prog( [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 8)).registre == 1000
        print "test73: ", intcode_machine( init_prog( [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 9)).registre == 1001
    # part 2
    prog_in = init_prog( puzzle, 5)
    while prog_in.state<>99:
        prog_in = intcode_machine( deepcopy(prog_in))
    print "Result 2: ", prog_in.registre


if day == 7: # FIXME still buggy
    print "Day is:", day
    # test
    IO_table = { 0:1, 1:2, 2:3, 3:4}
    print intcode_machine( init_prog([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], 4)).registre
    print intcode_machine( init_prog([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], 1)).registre
    print amplifiers( [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], 0, 0, [4,3,2,1,0], 4, IO_table)
    # part 1
    #puzzle = np.genfromtxt('input_2019_' + str(day), dtype='int', delimiter=',').tolist()
    #IO_table = { 0:1, 1:2, 2:3, 3:4}
    #print amplifiers( puzzle, 0, 0, [1,3,2,4,0], 4, IO_table) 
    #print amplifiers( puzzle, input_in=0, amp_in=0, phase_settings, amp_out=4, IO_table) 


if day == 9:
    print "Day is:", day
    puzzle = np.genfromtxt('input_2019_' + str(day), dtype='int', delimiter=',').tolist()
    prog_in = init_prog( puzzle, 1)
    prog_in = intcode_machine( deepcopy(prog_in))
    print "Result 1: ", prog_in.registre
    prog_in = init_prog( puzzle, 2)
    prog_in = intcode_machine( deepcopy(prog_in))
    print "Result 2: ", prog_in.registre

if day == 11:
    print "Day is:", day
    puzzle = np.genfromtxt('input_2019_' + str(day), dtype='int', delimiter=',').tolist()
    coords_painted = {} #, y, color
    robot = class_robot()
    # init prog - a black panel
    prog_in = init_prog( puzzle, np.nan)
    while prog_in.state <>99:
        # 0 - check color
        if robot.coords in coords_painted:
            prog_in.registre = coords_painted[ robot.coords][ 'color'] # 0 black 1 white
        elif np.isnan(prog_in.registre): # start panel
            prog_in.registre = 1 
        else:
            prog_in.registre = 0 
        # 1 - paint
        prog_in = intcode_machine( prog_in)
        first_out = prog_in.registre
        first_ind = prog_in.index
        if prog_in.state == 99:
            break
        coords_painted[ robot.coords] = {'color': prog_in.registre}
        # 2 - move
        prog_in = intcode_machine( prog_in)
        robot.way = robot.way + [-90, 90][ prog_in.registre]
        robot.coords = update_coords( robot.coords, robot.way)

print len( coords_painted)

matrix_2plot = np.zeros( (160,260))

for elt in coords_painted:
        matrix_2plot[ int(elt[0]+20), int(20+elt[1])] = coords_painted[elt]['color']

fig, ax = plt.subplots()
im = ax.imshow( matrix_2plot, interpolation='bilinear')
plt.savefig("img_solution_day11.png")


