import numpy as np
import copy
import matplotlib.pyplot as mplt

#############################
# day 6 - 1

# input
list_orbit_in = np.genfromtxt('input_2019_6', dtype='str', delimiter=',').tolist()

# list planet
list_planet = list()
for elt in list_orbit_in:
    pl1 = elt.split(')')[0]
    pl2 = elt.split(')')[1]
    if pl1 not in list_planet:
        list_planet.append( pl1)
    if pl2 not in list_planet:
        list_planet.append( pl2)
nb_planet = len(list_planet)

# direct orbit
matrix_orbit = np.zeros( (nb_planet, nb_planet))
for elt in list_orbit_in:
    pl1 = elt.split(')')[0]
    pl2 = elt.split(')')[1]
    matrix_orbit[ list_planet.index( pl1), list_planet.index( pl2)] = 1

# computation
me = list_planet.index('YOU')
fdp = list_planet.index('SAN')

bigM = 1e10
nb_steps=0
nb_orbit = matrix_orbit.sum()
nb_orbit_2add = -1

matrix_nb_steps = bigM * np.ones( (nb_planet, nb_planet)) # distance matrix
matrix_orbit_ind = copy.deepcopy( matrix_orbit) # indirect orbit
matrix_orbit_image = copy.deepcopy( matrix_orbit)

while nb_orbit_2add <> 0:
    # part1:
    matrix_orbit_ind = np.dot( matrix_orbit_ind, matrix_orbit)
    matrix_orbit_image+=matrix_orbit_ind
    nb_orbit_2add = matrix_orbit_ind.sum()
    nb_orbit += nb_orbit_2add
    # part2
    nb_steps += 1
    matrix_nb_steps[ matrix_orbit_ind> 0] = np.minimum(matrix_nb_steps[ matrix_orbit_ind> 0], nb_steps)

#print nb_orbit, min(matrix_nb_steps[:, me] + matrix_nb_steps[:, fdp])
print 'Day 6 : There is fucking ', nb_orbit, ' direct and indirect orbit in this system'
print 'Day 6 : and Santa, aka Fdp, is ', min(matrix_nb_steps[:, me] + matrix_nb_steps[:, fdp]), ' away. Should we go ?'


fig, ax = mplt.subplots()
matrix_orbit_image[ matrix_orbit_image== bigM] = np.nan
#im = ax.imshow(matrix_orbit_image, interpolation='bilinear', cmap=cm.RdYlGn)
im = ax.imshow(matrix_orbit_image, interpolation='bilinear')
mplt.show()

