import numpy as np
import copy
import sys

sys.setrecursionlimit(16000)

#############################
# day 6 - 1

list_orbit_in = np.genfromtxt('input_2019_6', dtype='str', delimiter=',').tolist()

list_planet = list()

for elt in list_orbit_in:
    pl1 = elt.split(')')[0]
    pl2 = elt.split(')')[1]
    if pl1 not in list_planet:
        list_planet.append( pl1)
    if pl2 not in list_planet:
        list_planet.append( pl2)

nb_planet = len(list_planet)
matrix_orbit = np.zeros( (nb_planet, nb_planet))


# list of list for direct orbit
list_orbit_rec = list()
nb_max = 0
for plt in list_planet:
    list_orbit_temp = [elt.split(')')[0] for elt in list_orbit_in if elt.split(')')[0] == plt ]
    list_orbit_rec.append( list_orbit_temp)
    nb_max = max(len(list_orbit_temp), nb_max)

def find_orbits( plt, list_orbit_rec, list_planet, orbit_temp):
    orbit_temp_new = copy.deepcopy(orbit_temp)
    index_planet = list_planet.index( plt)
    for next_planet in list_orbit_rec[ index_planet]:
        new_orbit = [plt, next_planet]
        if not new_orbit in orbit_temp_new:
            orbit_temp_new.append( new_orbit)
        for new_orbit in find_orbits( next_planet, list_orbit_rec, list_planet, orbit_temp_new):
            if not new_orbit in orbit_temp_new:
                orbit_temp_new.append( new_orbit)
    return orbit_temp_new
        


# no cleaning necessary
nb_orbit = 0
list_dirundir_orbit = list()
for plt in list_planet:
    orbit_temp = list()
    orbit_temp = find_orbits( plt, list_orbit_rec, list_planet, orbit_temp)
    for elt in orbit_temp:
        if not elt in list_dirundir_orbit:
            list_dirundir_orbit.append(elt)
            nb_orbit +=1
