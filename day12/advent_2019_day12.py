import numpy as np
from copy import deepcopy
import matplotlib.pyplot as mplt
import time

#############################
# day 12 - 1

# input
#list_orbit_in = np.genfromtxt('input_2019_6', dtype='str', delimiter='').tolist()

def gravity(moons):
    moons_out = dict((moons))
    for moon1 in moons:
        for pos in moons[moon1][ 'coords']:
            for moon2 in moons:
                if moon2 <> moon1:
                    cond = (moons[moon1][ 'coords'][pos] <= moons[moon2][ 'coords'][pos]) + (moons[moon1][ 'coords'][pos] == moons[moon2][ 'coords'][pos])
                    moons_out[moon1]['vel'][pos] = moons_out[moon1]['vel'][pos] + [-1, +1, 0][  cond]
    return moons_out

def velocity(moons):
    for moon1 in moons:
        for pos in moons[moon1][ 'coords']:
            moons[moon1]['coords'][pos] += moons[moon1]['vel'][pos]
    return moons

def energy( moons):
    energy0 = 0
    for moon1 in moons:
        kin = 0
        pot = 0
        for pos in moons[moon1][ 'coords']:
            pot += np.abs( moons[moon1]['coords'][pos])
            kin += np.abs( moons[moon1]['vel'][pos])
        energy0 += pot*kin
    return energy0

def decode_coords( stringy):
    coords = list()
    for ii in range(3):
        coords.append( int(stringy.split(',')[ii].split('=')[1].replace('>','')))
    return coords

def build_moons(input_in):
    list_orbit_in = np.genfromtxt(input_in, dtype='str', delimiter='|||').tolist()
    moons = {}
    ii= -1
    for line in list_orbit_in:
        ii +=1
        x, y, z = decode_coords( line)
        moons[ moons_name[ii]] = { 'coords': {'x':x, 'y':y, 'z':z}, 'vel': {'x':0, 'y':0, 'z':0}}
    return moons

moons_name = ['Io','Europa','Ganymede','Callisto']
# test 
moons = build_moons('test1')
for ii in range(10):
    #print moons
    moons = gravity(moons)
    moons = velocity( moons)
print energy( moons)

# part 1
moons = build_moons('input_2019_12')
for ii in range(1000):
    #print moons
    moons = gravity(moons)
    moons = velocity( moons)

def listcoords(moons):
    listy = list()
    for moon in moons:
        for pos in moons[moon]['coords']:
            listy.append( moons[moon]['coords'][pos])
    return listy

# part 2 : moon by moon
start_time = time.time()
for moon_name in [1]: 
    moons = build_moons('input_2019_12')
    #moons = build_moons('test1')
    trajectory = {}
    steps = 0
    while True:
        #tuply = ( moons[ moon_name]['coords']['x'], moons[ moon_name]['coords']['y'], moons[ moon_name]['coords']['z'])
        allcoords = listcoords(moons)
        if not ener in trajectory:
            listy = list()
            listy.append(  allcoords)
            trajectory[ ener] = listy
        else:
            toto=False
            for elt in trajectory[ ener]:


:q


                if elt == allcoords:
                    toto = True
            #if allcoords in trajectory[ ener]:
            if toto:
                break
            else:
                listy = [elt for elt in trajectory[ ener]]
                listy.append( allcoords)
                trajectory[ ener] = listy
        if steps % 100000 == 0:
            print '-------->>>>>>  ', np.around(float(steps) / 4686774924 * 100, 4), time.time() - start_time
        moons = gravity(moons)
        moons = velocity( moons)
        ener = energy( moons)
        steps +=1

print steps -1

# idead : for gravity dont list all
# relative positon to satelite 1 ?
# not energy, but distance of one

# condition does not tak a lot of time at first

"""
matrice coords
[x,y,z, sat]
1 2 4 8, 15
sum sur axis sat 15

a*b
construct b from velocity and a
    010
    010
    010
100

a * [1? 2? 3] .sum() give index
veloc[ index] = 111

compute velocity :
 
    1
    2
    3
100 
-
001


n a 4 sat :
4*4 = 16


incertitudes par secteur : 
    sur l'objectif lui même : se prononce sur al gravité et le risque de ne pas atteindre.
objectif neutralité : pour respecter l'obectif Paris
"""

