import numpy as np
from copy import deepcopy
import matplotlib.pyplot as mplt
import time

#############################
# day 12 - 1

def x_move(x, xvel):
    xvel_new = xvel - [vel_vect[ ((x[ii] >=x).sum()-1)*2 - ((x[ii] ==x).sum()-1)] for ii in range(4)]
    x = x + xvel_new
    return x, xvel_new

def decode_coords( stringy):
    coords = list()
    for ii in range(3):
        coords.append( int(stringy.split(',')[ii].split('=')[1].replace('>','')))
    return coords

def build_moons(input_in):
    moons = np.zeros( (3, 4))
    list_orbit_in = np.genfromtxt(input_in, dtype='str', delimiter='|||').tolist()
    moons = np.zeros( (3, 4))
    ii= -1
    for line in list_orbit_in:
        ii +=1
        x, y, z = decode_coords( line)
        moons[ :,ii] = [x, y, z]
    return moons[0], moons[1], moons[2]

def x_move(x, xvel):
    xvel_new = xvel - [vel_vect[ ((x[ii] >=x).sum()-1)*2 - ((x[ii] ==x).sum()-1)] for ii in range(4)]
    x = x + xvel_new
    return x, xvel_new

x, y, z = build_moons('test1')
x, y, z = build_moons('input_2019_12')

y_start = deepcopy(y)
yvel = np.zeros((4))
x_start = deepcopy(x)
xvel = np.zeros((4))
z_start = deepcopy(z)
zvel = np.zeros((4))

vel_vect = [-3,-2,-1,0,1,2,3]

# test 
start_time = time.time()

ii = -1
while ii <46800000:
#for ii in range(10):
    ii +=1
    #print moons
    x, xvel = x_move(x, xvel)
    #y, yvel = x_move(y, yvel)
    #z, zvel = x_move(z, zvel)
    #if ((x -x_start)==0).sum()==4:
    if ((x-x_start)==0).sum() ==4:
    #if ((y-y_start)==0).sum()==4:
    #if ((z-z_start)==0).sum() ==4:
                print 'found', ii, x, y, z
                break
    #print (x -x_start).sum()
    if ii % 100000 == 0:
            print '-------->>>>>>  ', np.around(float(ii) / 4686774924 * 100, 4), time.time() - start_time

print ii +2 

# for x, y, z we got
#193052, 231614, 84032

#then solving by hand :
#In [91]: 193052* 231614* 84032
#Out[91]: 3757368691421696
#/8

