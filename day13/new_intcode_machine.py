
from operator import add, mul, ne, gt, lt, eq
import numpy as np
from copy import deepcopy 
import matplotlib.pyplot as plt
import sys, tty,termios
import time

day = 13
do_tests = True
increase_memory = 10000

opcode_ins = { 1: {'oper': add, 'len_code': 3, 'typ_val': 'oper'},
               2: {'oper': mul, 'len_code': 3, 'typ_val': 'oper'},
               3: {             'len_code': 1, 'typ_val': 'Input'},
               4: {             'len_code': 1, 'typ_val': 'Output'},
               5: {'oper': ne,  'len_code': 2, 'typ_val': 'cond_noelse0'},
               6: {'oper': eq,  'len_code': 2, 'typ_val': 'cond_noelse0'},
               7: {'oper': lt,  'len_code': 3, 'typ_val': 'cond_else', 'val': [0,1]},
               8: {'oper': eq,  'len_code': 3, 'typ_val': 'cond_else', 'val': [0,1]},
               9: {'oper': '',  'len_code': 1, 'typ_val': '' },
               99: {'len_code': 0,'typ_val': ''}}

class class_program:
    prog = list()
    index = np.nan
    relat = np.nan
    state = np.nan
    registre = np.nan

def set_registre(self, inputin):
    self.registre = inputin
    return self

def init_prog( prog, inputy=0):
    progy = class_program()
    progy.prog = prog
    # add memory - should be dynamic
    for ii in range(increase_memory):
      progy.prog = progy.prog + [np.nan]
    progy.index = 0
    progy.relat = 0
    progy.registre = inputy
    return progy

def set_param_indexes(mod, i, program):
    if mod == 0:
        param = program.prog[ program.index+i]
    elif mod == 1:
        param = program.index+i
    elif mod == 2:
        param = program.relat + program.prog[ program.index+i]
    return param 

def decode_opcode(program):
    ins = program.prog[ program.index]
    opcode = ins % 100
    modes = [ int(ii) for ii in ("%05d" % ins)[0:-2] ]
    parameters = [set_param_indexes( modes[-ii-1], ii+1, program) for ii in range( opcode_ins[opcode]['len_code'])]
    return opcode, parameters

def intcode_machine( program):
    opcode = -1
    while opcode < 99 and opcode <>4:
        opcode, parameters = decode_opcode(program) 
        #print opcode
        # parameters indices
        # c is always the index where to assign a value
        if opcode_ins[opcode]['len_code'] == 1:
            c =       [ parameters[i] for i in range(1)][0]
        elif opcode_ins[opcode]['len_code'] == 2:
            a, c =    [ parameters[i] for i in range(2)]
        elif opcode_ins[opcode]['len_code'] == 3:
            a, b, c = [ parameters[i] for i in range(3)]
        # compute the operation
        if opcode_ins[opcode]['len_code'] == 3: # 1 2 8 7 
            operation = opcode_ins[opcode]['oper']( program.prog[ a], program.prog[ b])
        if opcode_ins[opcode]['typ_val'] == 'cond_noelse0':
            operation = opcode_ins[opcode]['oper']( program.prog[ a], 0)
        # actuel job
        if opcode_ins[opcode]['typ_val'] == 'oper': # case 1, 2
            program.prog[ c] = operation
        if opcode_ins[opcode]['typ_val'] == 'Input': # case 3
            program.prog[ c] = 1* program.registre
            #program.registre = np.nan # we empty the registre. we assume it is red only once
        if opcode_ins[opcode]['typ_val'] == 'Output': # case 4
            program.registre = 1* program.prog[ c]
        if 'cond_else' == opcode_ins[opcode]['typ_val']: # case 7, 8
            program.prog[ c] = opcode_ins[opcode]['val'][ operation]
        if 'cond_noelse0' == opcode_ins[opcode]['typ_val']: # case 5 6
            if operation: 
                jump = - ( program.index + opcode_ins[opcode]['len_code'] + 1) + program.prog[ c]
            else:
                jump = 0
        else: # case not 5 6
            jump = 0
        if opcode == 9:
            program.relat = program.relat + program.prog[ c]
        program.index = program.index + opcode_ins[opcode]['len_code'] + 1 + jump
        program.state = opcode
        if program.state == 4:
            break
        if program.state == 99:
            break
        #    program.registre = np.nan
    return program

##########################################
###  Days computation
##########################################

tile2symbol = {0: ' ', 1: 'W', 2:'.', 3:'_', 4:'o'}

# from https://stackoverflow.com/questions/22397289/finding-the-values-of-the-arrow-keys-in-python-why-are-they-triples
class _Getch:       
    def __call__(self):
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            return ch

def game_onestep( prog_in, game_state):
    for ii_out in range(3):
        if prog_in.state == 99:
            break
        prog_in = intcode_machine( prog_in)
        if ii_out == 0:
            x=prog_in.registre
        elif ii_out == 1:
            y=prog_in.registre
        elif ii_out == 2:
            tilid = prog_in.registre
            #game[ (x,y)] = tilid
            if x==-1 and y == 0 and tilid>0:
                print '\n **** YOUR SCORE IS : ', tilid, ' ****\n'
            else:
                game_state[ x, y] = tilid
    if False:
        plot_game( game_state)
    return prog_in, game_state  

def plot_game(mat):
   xlen, ylen = mat.shape[0], mat.shape[1]
   print '------------------------------------------------------------------------------------------------------'
   for jj in range(ylen):
       str2print = ''
       for ii in range(xlen):
           str2print = str2print + tile2symbol[ mat[ii,jj]] + ' '
       print str2print
   return 0

def play_all_game(prog_in, mat_game, nb_game):
    new_pos = 0
    #prog_in, mat_game = game_onestep( prog_in, mat_game)
    prog_in.registre=0
    while prog_in.state <>99:
        prog_in, mat_game = game_onestep( prog_in, mat_game)
        #plot_game( mat_game)
        if prog_in.registre == 4:
            plot_game( mat_game)
            paddle_pos = np.where(mat_game == 3)[0]
            y_pos_ball = np.where(mat_game == 4)[1]
            x_pos_ball = np.where(mat_game == 4)[0]
            if paddle_pos > x_pos_ball:
                    new_pos = -1
            elif paddle_pos < x_pos_ball:
                    new_pos = 1
            elif paddle_pos == x_pos_ball:
                    new_pos = 0
            prog_in.registre = new_pos*1
            #print 'paddle - ball - reg : ', paddle_pos, x_pos_ball, prog_in.registre
            #time.sleep(1)
    return (mat_game == 2).sum(), paddle_pos
#np.where(mat_game == 4)[0], np.where(mat_game == 3)[0]


if day == 13:
    print "Day is:", day
    puzzle = np.genfromtxt('input', dtype='int', delimiter=',').tolist()
    prog_in = init_prog( puzzle, np.nan)
    #part 1
    game = {}
    mat_game = np.zeros( (38,22))
    ii_out = 0
    nb_block = 0
    while prog_in.state <>99:
        prog_in = intcode_machine( prog_in)
        if ii_out == 0:
            x=prog_in.registre
            ii_out +=1
        elif ii_out == 1:
            y=prog_in.registre
            ii_out +=1
        elif ii_out == 2:
            tilid = prog_in.registre
            ii_out =0
            game[ (x,y)] = tilid
            mat_game[ x, y] = tilid
            if tilid == 2:
                nb_block += 1
            if tilid == 3:
                paddle = [x,y]
                print "paddle position is", paddle

if day == 13:
    print "Day is:", day
    prog_in = init_prog( puzzle, np.nan)
    prog_in.prog[0] = 2 # playing for free
    #amat_game = np.zeros( (38,22))
    nb_block = 1
    nb_game = -1
    nb_block, y_final_ball = play_all_game( deepcopy(prog_in), deepcopy(mat_game), nb_game)   

# -1,0,12345 is the score



# aurelien, resto
# toi aca va ?
# cadeau gautheir, gaelle 
