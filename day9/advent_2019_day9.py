import numpy as np
import copy

#############################
# day 9 - 1&2

alarm = np.genfromtxt('input_2019_9', dtype='int', delimiter=',').tolist()
increase_memeory = 1000


def decode_upcode( digit):
    stry = '0000' + str(digit)
    opcode = int(stry[-2:])
    error=False
    list_out = list()
    for elt in stry[0:-2]:
        if elt in ['0','1','2']:
            #list_out.append(bool(int(elt))) # no more boolean
            list_out.append(int(elt))
        else:
            list_out.append(elt)
            error = True
    list_out.reverse()
    return opcode, list_out, error

def get_indexes( alarm, list_op, index, relat_base, nb_ind):
    list_ind=list()
    for ii in range(nb_ind):
        if list_op[ii]==0:
            list_ind.append( alarm[index+ii+1])
        elif list_op[ii]==1:
            list_ind.append( index+ii+1)
        elif list_op[ii]==2:
            list_ind.append( relat_base+alarm[index+ii+1])
    return list_ind

def opcoding( alarm, input0, relat_base0=0):
  input_in = input0
  index = 0
  wrong = False
  output = input0 # np.nan
  ii=-1
  relatbase = relat_base0
  # add memory
  for ii in range(increase_memeory):
      #alarm = [np.nan] + alarm + [np.nan]
      alarm = alarm + [np.nan]
  #index = index + increase_memeory
  while alarm[index] <> 99:
    pointerModified = False # by default, exception for opcode 5
    pointerValue = 0
    ii +=1
    opcode,list_op, error = decode_upcode( alarm[index])
    #print opcode, index, alarm[index]
    if error:
        print 'Error with: ', alarm[index], index
        break
    if opcode == 1:
        #ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        ind0, ind1, ind2 =  get_indexes( alarm, list_op, index, relatbase, 3)
        alarm[ ind2] = alarm[ind0] + alarm[ind1]
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
        #alarm[ ind2] = alarm[ind0] + alarm[ind1]
    elif opcode == 2:
        #ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        ind0, ind1, ind2 =  get_indexes( alarm, list_op, index, relatbase, 3)
        alarm[ ind2] = alarm[ind0] * alarm[ind1]
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
        #alarm[ ind2] = alarm[ind0] * alarm[ind1]
    elif opcode == 3:
        ind2 =  get_indexes( alarm, list_op, index, relatbase, 1)
        ind2 = ind2[0]
        #if list_op[0]:
        #    ind2 = index+1
        #else:
        #    ind2 = alarm[index+1]
        alarm[ ind2] = input_in
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
        #alarm[ ind2] = input_in
    elif opcode == 4:
        if list_op[0]==1:
            output = alarm[ index+1]
        elif list_op[0]==0:
            output = alarm[ alarm[index+1]]
        elif list_op[0]==2:
            output = alarm[ relatbase+alarm[index+1]]
        print 'Output: ', output
        input_in = output
    elif opcode == 5:
        #ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        ind0, ind1 =  get_indexes( alarm, list_op, index, relatbase, 2)
        if alarm[ ind0] <>0:
            #pointerValue = alarm[ index]
            #alarm[ index] = alarm[ ind1]
            pointerModified = True
            pointerValue = alarm[ ind1] # FIXME
            #pointerValue = alarm[ index]
        else:
            pointerModified = False
    elif opcode == 6:
        #ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        ind0, ind1 =  get_indexes( alarm, list_op, index, relatbase, 2)
        if alarm[ ind0] ==0:
            #pointerValue = alarm[ index]
            #alarm[ index] = alarm[ ind1]
            pointerModified = True
            pointerValue = alarm[ ind1] # FIXME
            #pointerValue = alarm[ index]
        else:
            pointerModified = False
    elif opcode == 7:
        #ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        ind0, ind1, ind2 =  get_indexes( alarm, list_op, index, relatbase, 3)
        #pointerValue = alarm[ index]
        if alarm[ ind0] < alarm[ ind1]:
            alarm[ ind2] = 1
        else:
            alarm[ ind2] = 0
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[ index]
    elif opcode == 8:
        #ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        ind0, ind1, ind2 =  get_indexes( alarm, list_op, index, relatbase, 3)
        #pointerValue = alarm[ index]
        if alarm[ ind0] == alarm[ ind1]:
            alarm[ ind2] = 1
        else:
            alarm[ ind2] = 0
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[ index]
    elif opcode == 9:
        if list_op[0]==1:
            ind2 = index+1
        elif list_op[0]==0:
            ind2 = alarm[index+1]
        elif list_op[0]==2:
            ind2 = relatbase + alarm[index+1]
        #print relatbase, alarm[index+1],  ind2, alarm[ ind2]
        relatbase = relatbase + alarm[ ind2]
    elif opcode <> 99:
        wrong = True
        break
    index_sav = index*1
    if opcode in [1,2,99,7,8]:
        index = index+4
    elif opcode in [5,6]:
        index = index+3
    elif opcode in [3,4,9]:
        index = index+2
    if pointerModified:
        index = pointerValue
        #print opcode, index, alarm[index], pointerModified
  if wrong:
      return np.nan
  else:
      return output, alarm

#print "test 2: ", opcoding([104,1125899906842624,99], 1)
#print "test 1: ", opcoding([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99], 1, 0)
#print "test 3: ", opcoding([1102,34915192,34915192,7,4,7,99,0], 1)

test = copy.deepcopy( alarm)
output, alarm2 = opcoding(test, 1)
print output
print ''
#print "test 1: ", opcoding([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99], 1, 0)
test = copy.deepcopy( alarm)
opcoding(test, 2)

