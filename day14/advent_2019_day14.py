import numpy as np
import copy
import matplotlib.pyplot as mplt
from scipy.optimize import fsolve

#############################
# day 6 - 1

# input
def list_reaction( input_file):
    list_reactions = np.genfromtxt( input_file, dtype='str', delimiter='|').tolist()
    # list_chemicals
    list_chemicals = list()
    for elt in list_reactions:
        left = elt.split(' => ')[0]
        right = elt.split(' => ')[1]
        for chi in left.split(', '):
            list_chemicals.append( chi.split(' ')[1])
        for chi in right.split(', '):
            list_chemicals.append( chi.split(' ')[1])
    list_chemicals = list(set(list_chemicals)) 
    nb_chi = len( list_chemicals)

    # matrice of reactions - producing j requires mat'i,j) of the i-th element
    mat_react = np.zeros( (nb_chi,nb_chi))

    mat_react_out = np.zeros( (nb_chi,1))
    for elt in list_reactions:
        left = elt.split(' => ')[0]
        right = elt.split(' => ')[1]
        j = list_chemicals.index( right.split(', ')[0].split(' ')[1])
        num_j = right.split(', ')[0].split(' ')[0]
        for chi in left.split(', '):
            i = list_chemicals.index( chi.split(' ')[1])
            mat_react[ i,j] = float( chi.split(' ')[0])
        mat_react_out[ j] = float( num_j)
    return list_chemicals, mat_react, mat_react_out

input_file = 'test1'
input_file = 'test2'
input_file = 'test3'
input_file = 'test4'
input_file = 'input'
list_chemicals, mat_react, mat_react_out = list_reaction( input_file)


def howmuchore( fuel):
    x = np.zeros( ( len(list_chemicals),1))
    x[ list_chemicals.index('FUEL')] = fuel

    stock = 0 * x
    stock_increase = np.ones((1))

    stop = False

    x_cum = 0*x
    while not stop:
        nb_react = x * 0
        nb_react[ x>0] =  ( np.ceil(x / mat_react_out))[ x>0] 
        nb_react[ np.isnan( nb_react)] = 0
        nb_react[ nb_react==np.inf] = 0
        # requirement less what produces a reactions to zero and stock
        #print x, nb_react, mat_react_out * nb_react, '\n\n'
        x -= mat_react_out * nb_react
        stock[ x<0] = -x[ x<0]
        x [ x<0] = 0
        #x = 0*x
        # new requirements
        x += np.dot( mat_react, nb_react)
        x_cum += x
        x_sav = x*1
        x = x - stock
        x[ x<0] = 0
        stock =  stock - np.minimum(x_sav, stock)
        stop = (x.sum() - x[ list_chemicals.index('ORE')]) == 0
        #print x[ list_chemicals.index('ORE')]
    return x[ list_chemicals.index('ORE')]


def howmuchfuel_for_1e12(fuel):
    #fuel = np.floor(fuel)
    return howmuchore( fuel) - 1e12

print 'Results part 1: ', howmuchore( 1)

xsol = fsolve( howmuchfuel_for_1e12, 2e6)
xsol = np.floor(xsol) - 1

