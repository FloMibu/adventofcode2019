import numpy as np

# rule 1:     Two adjacent digits are the same (like 22 in 122345).
# rule 2:     Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
# those two rules can be tested through matricial product and computation. Ex, for rule 1 : suming column 2 by 2
# note : using matrix computation is useless, unefficient, but fun 

def code2matrix( code, basis):
    a = np.zeros( (basis,code.shape[0]))
    for ii in range( code.shape[0]):
        a[ code[ii],ii] = 1
    return a

def matrix2code( mat_code, mat_rule2):
    return np.dot( mat_rule2, mat_code).sum(axis=0)

def code2num( code, basis):
    a = 0
    for ii in range( code.shape[0]):
        a += code[ii] * (basis)** (code.shape[0]-ii-1)
    return a

def num2code( num):
    stry = str(int(num))
    str2exec = 'a = np.array( ['
    for ii in range(len( stry)):
        str2exec += stry[ii] + ','
    str2exec += '])'
    exec( str2exec)
    return a

def increase_matrix_code( mat_code, basis, mat_rule2):
    num = int( code2num( matrix2code(mat_code, mat_rule2), basis) +1) # here we assume the numarical basis is 10
    return code2matrix( num2code( num), basis)

def rule1( mat_code, mat_rule1):
    aa = np.dot( mat_code, mat_rule1)
    return np.where( aa==2)[0].shape[0] <> 0

def rule2( mat_code, mat_rule2, mat_rule1):
    aa = np.dot(mat_rule2, mat_code).sum(axis=0)
    bb = np.dot( aa, mat_rule1) /2
    return ( (aa[1:] >= bb).sum() == mat_rule1.shape[1] )

#357253-892942
max_code = np.array( [8, 9, 2, 9, 4, 2])
try_code = np.array( [3, 5, 7, 2, 5, 3])

nb_digit = max_code.shape[0]
numerical_basis = 10

mat_rule1 = np.zeros( ( nb_digit, nb_digit-1 ))
for ii in range( nb_digit-1):
    mat_rule1[ ii:(ii+2), ii] = [1,1]

mat_rule2 = np.zeros( (numerical_basis,numerical_basis))
mat_rule2[ range(numerical_basis), range(numerical_basis)] = np.linspace(0,numerical_basis-1,numerical_basis)

# I choose not to test first code
nb_steps = code2num( max_code, numerical_basis) - code2num( try_code, numerical_basis)
nb_good_code = 0
mat_code = code2matrix( try_code, numerical_basis)
for ii in range(nb_steps):
    mat_code = increase_matrix_code( mat_code, numerical_basis, mat_rule2)
    if rule1( mat_code, mat_rule1) and rule2( mat_code, mat_rule2, mat_rule1):
        nb_good_code += 1

print "Day 4 : the number of valid code might be : ", nb_good_code 

# PART 2
def rule3( mat_code, mat_rule1):
    aa = np.dot( mat_code, mat_rule1)
    bb = np.dot( aa, mat_rule1[1:, 1:])
    cc = np.dot( bb, mat_rule1[2:, 2:])
    dd = np.dot( cc, mat_rule1[3:, 3:])
    ee = np.dot( dd, mat_rule1[4:, 4:])
    return np.where( ee==6)[0].shape[0] <>0 or np.where( ee==15)[0].shape[0] <>0 or np.where( ee==20)[0].shape[0] <>0

mat_rule3 = np.zeros( ( nb_digit-1, nb_digit-2 ))
for ii in range( nb_digit-2):
    mat_rule3[ ii:(ii+2), ii] = [1,1]

nb_good_code = 0
mat_code = code2matrix( try_code, numerical_basis)
for ii in range(nb_steps):
    mat_code = increase_matrix_code( mat_code, numerical_basis, mat_rule2)
    if rule2( mat_code, mat_rule2, mat_rule1) and rule3(mat_code, mat_rule1):
        nb_good_code += 1

print "Day 4 : but in fact it is ", nb_good_code 
