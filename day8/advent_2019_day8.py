import numpy as np
import copy
import matplotlib.pyplot as mplt

#############################
# day 6 - 1

# input
input_in = np.genfromtxt('input_2019_8', dtype = 'str', delimiter='').tolist()

# recontruct
nb_num = len(input_in)

wide = 25
tall = 6
len_layer = (wide * tall) 
nb_layers = nb_num // len_layer


list_char = list()
nb_zero = 1e10
product12 = np.nan


list_layer = list()
for ii in range(nb_layers):
    elt = input_in[ (ii*len_layer):( (ii+1)*len_layer)]
    list_layer.append( elt)
    #print ii, elt.count( '0'), elt
    if elt.count( '0') < nb_zero:
        nb_zero = elt.count( '0')
        print ii, elt.count( '0')
        product12 = elt.count( '1') * elt.count( '2')

def layer2matrix( layer):
    mat = np.zeros( (tall, wide) )
    for kk in range(len_layer):
        ii = (kk) // (wide )
        jj = (kk) % (wide )
        print ii, jj
        mat[ii,jj] = int(layer[kk])
    return mat

layer_image = layer2matrix( list_layer[0])

for ii in range( len(list_layer)-1):
    image_temp = layer2matrix( list_layer[ii+1])
    layer_image[ layer_image==2] = image_temp[ layer_image==2]


fig, ax = mplt.subplots()
im = ax.imshow(layer_image, interpolation='bilinear')
#im = ax.imshow(layer_image)
mplt.show()

