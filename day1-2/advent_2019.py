import numpy as np

#############################
# day 1 - 1
modules = np.genfromtxt('input_2019_1')
mass = ((modules // 3) -2).sum()
print "Day 1 : use ", mass, " of fuel to save Santa"

# day 1 - 2
def fuel( mass_in):
  mass_out = ((mass_in // 3) -2)
  mass_out[ mass_out <0] = 0
  return mass_out


fuel_add = fuel( np.genfromtxt('input_2019_1'))
fuel_tot = fuel_add

while fuel_add.sum() <>0:
    fuel_add = fuel( fuel_add)
    fuel_tot = fuel_tot + fuel_add

print "Day 1 : Oh no, you had to fuel the fuel, use ", fuel_tot.sum(), " of fuel to save Santa"

#############################
# day 2 - 1

alarm = np.genfromtxt('input_2019_2', dtype='int', delimiter=',').tolist()

def opcoding( alarm, noun, verb):
  alarm[1] = noun
  alarm[2] = verb
  index = 0
  wrong = False
  while alarm[index] <> 99:
    if alarm[index] == 1:
        alarm[ alarm[index+3]] = alarm[ alarm[index+2]] + alarm[ alarm[index+1]]
    elif alarm[index] == 2:
        alarm[ alarm[index+3]] = alarm[ alarm[index+2]] * alarm[ alarm[index+1]]
    elif alarm[index] <> 99:
        wrong = True
        break
    index = index+4
  if wrong:
      return np.nan
  else:
      return alarm[0]

print "Day 2 : intiating the opcode...", opcoding(alarm, 12, 2)

# day 2 - 2
value = 0
for noun in range( 99):
    if value == 19690720:
        break
    for verb in range( 99):
        # ugly csv reaing in loop :)
        alarm = np.genfromtxt('input_2019_2', dtype='int', delimiter=',').tolist()
        value = opcoding(alarm, noun, verb)
        if value == 19690720:
          break
        
print "Day 2 : gravity is ", 100 * (noun-1) + verb

