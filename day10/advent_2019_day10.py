import numpy as np
import copy
import matplotlib.pyplot as mplt

#############################
# day 10 

# input
input_in = np.genfromtxt('input_2019_10', dtype = 'str', delimiter='', comments='|')
hei = input_in.shape[0]
wid = len(input_in[0])


# recontruct
asteroids = np.nan * np.zeros( (wid, hei))
for ii in range( hei):
    line = np.array( [elt for elt in input_in[ii]] )
    asteroids[ ii,  line == '#'] = int(1)
    asteroids[ ii,  line == '.'] = int(0)

def ang_len( iss, jss, iaa, jaa): # compute angles and distances
    leng = (iss-iaa)**2 + (jss-jaa)**2
    angl = np.arctan2( jss-jaa, iss-iaa) * 180.0 / np.pi
    angl = np.around( [ -angl, -angl+360][ -angl<0], 4)
    return str( angl), leng

def comp_pot_vision( iss, jss, hei, wid, asteroids):
    one_vision = {}
    for iaa in range(hei):
        for jaa in range(wid):
            angl, leng = ang_len(iss, jss, iaa, jaa)
            if leng <> 0:
                if (angl in one_vision):
                    if one_vision[angl]['len'] > leng  and asteroids[ iaa, jaa] > 0: 
                        ast_temp = one_vision[angl][ 'ast'] + asteroids[ iaa, jaa]
                        del one_vision[angl]
                        one_vision[angl] = {'len': leng, 'x': iaa, 'y': jaa, 'ast': ast_temp}
                elif asteroids[ iaa, jaa] > 0:
                    one_vision[angl] = {'len': leng, 'x': iaa, 'y': jaa, 'ast': asteroids[ iaa, jaa]}
    return one_vision

potential_vision = {}

for iss in range(hei):
    for jss in range(wid):
        if asteroids[ iss, jss]==1:
            potential_vision[ iss*(wid+1) + jss] = comp_pot_vision( iss, jss, hei, wid, asteroids)

max_ast = 0
for iss in range(hei):
    for jss in range(wid):
        nb_asteroids = 0
        if (iss*(wid+1) + jss) in potential_vision:
            for angl in potential_vision[ iss*(wid+1) + jss]:
                if potential_vision[ iss*(wid+1) + jss][angl]['ast'] > 0:
                    nb_asteroids +=1
        if nb_asteroids > max_ast:
            max_ast = nb_asteroids
            print max_ast
            i_sta, j_sta, station = iss, jss, iss*(wid+1) + jss

#part2
angles = list()
for pot in potential_vision:
    for ang in potential_vision[ pot]:
        angles.append( np.around(float(ang),4))
angles= sorted( list( set(angles)))

destroyed = 0

nb_ast = asteroids.sum()

while asteroids.sum() <> 1: # destroying all
    potential_vision = {}
    potential_vision[ station] = comp_pot_vision( i_sta, j_sta, hei, wid, asteroids) # recompute the vision every clock round
    for ang in angles:
        if str(ang) in potential_vision[ station]:
            asteroids[ potential_vision[ station][ str(ang)]['x'], potential_vision[ station][ str(ang)]['y']] = 0
            destroyed +=1
            #print destroyed, asteroids.sum(), ang
            if destroyed == 200:
                print potential_vision[ station][ str(ang)]['y'] * 100 + potential_vision[ station][ str(ang)]['x']

