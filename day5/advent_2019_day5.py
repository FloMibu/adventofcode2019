import numpy as np
import copy

#############################
# day 5 - 1

alarm = np.genfromtxt('input_2019_5', dtype='int', delimiter=',').tolist()


def decode_upcode( digit):
    stry = '0000' + str(digit)
    opcode = int(stry[-2:])
    error=False
    list_out = list()
    for elt in stry[0:-2]:
        if elt in ['0','1']:
            list_out.append(bool(int(elt)))
        else:
            list_out.append(elt)
            error = True
    list_out.reverse()
    #print opcode, list_out, error
    return opcode, list_out, error

def opcoding( alarm, noun, verb, input0):
  input_in = input0
  index = 0
  wrong = False
  output = np.nan
  ii=-1
  while alarm[index] <> 99:
    ii +=1
    print ii
    #print alarm
    opcode, list_op, error = decode_upcode( alarm[index])
    if error:
        print 'Error with: ', alarm[index], index
        break
    #print output, opcode, list_op[2]
    if opcode == 1:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        alarm[ ind2] = alarm[ind0] + alarm[ind1]
        #    print 'ERROR immediate mode'
        #    alarm[ index+3] = alarm[ alarm[index+2]] * (not list_op[1]) + alarm[index+2] * list_op[1] + alarm[ alarm[index+1]]  * (not list_op[0]) + alarm[index+1] * list_op[0]
    elif opcode == 2:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        alarm[ ind2] = alarm[ind0] * alarm[ind1]
    elif opcode == 3:
        input_in = input0 #int(input('Number: '))
        if list_op[0]:
            alarm[ index+1] = input_in
        else:
            alarm[ alarm[index+1]] = input_in
    elif opcode == 4:
        if list_op[0]:
            output = alarm[ index+1]
        else:
            output = alarm[ alarm[index+1]]
        print 'Output: ', output
        input_in = output
    elif opcode <> 99:
        wrong = True
        break
    #if alarm[index] == 4:
    #    output = alarm[ alarm[index+1]]
    #    input_in = output
    #else
    #    output = np.nan
    #print index, alarm[ index: index+8]
    if opcode in [1,2,99]:
        index = index+4
    elif opcode in [3,4]:
        index = index+2
  if wrong:
      return np.nan
  else:
      return alarm, error, alarm[0]

# tests
# print(int_code([3,0,4,0,99])) # print(int_code([1002,4,3,4,33])) # print(int_code([1101,100,-1,4,0]))

print opcoding( [3,0,4,0,99], 12,2,1)
print opcoding( [1002,3,4,3,33], 12,2,1)
print opcoding( [1101,100,-1,4,0], 12,2,1)
test = copy.deepcopy( alarm)
opcoding(test, 12, 2, 1)


