import numpy as np
import copy

#############################
# day 5 - 1

alarm = np.genfromtxt('input_2019_5', dtype='int', delimiter=',').tolist()

def decode_upcode( digit):
    stry = '0000' + str(digit)
    opcode = int(stry[-2:])
    error=False
    list_out = list()
    for elt in stry[0:-2]:
        if elt in ['0','1']:
            list_out.append(bool(int(elt)))
        else:
            list_out.append(elt)
            error = True
    list_out.reverse()
    return opcode, list_out, error

def opcoding( alarm, input0):
  input_in = input0
  index = 0
  wrong = False
  output = 8888 # np.nan
  ii=-1
  while alarm[index] <> 99:
    pointerModified = False # by default, exception for opcode 5
    pointerValue = 0
    ii +=1
    #print alarm
    opcode,list_op, error = decode_upcode( alarm[index])
    if error:
        print 'Error with: ', alarm[index], index
        break
    if opcode == 1:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        alarm[ ind2] = alarm[ind0] + alarm[ind1]
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
    elif opcode == 2:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        alarm[ ind2] = alarm[ind0] * alarm[ind1]
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
    elif opcode == 3:
        if list_op[0]:
            ind2 = index+1
        else:
            ind2 = alarm[index+1]
        alarm[ ind2] = input_in
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[index]
    elif opcode == 4:
        if list_op[0]:
            output = alarm[ index+1]
        else:
            output = alarm[ alarm[index+1]]
        print 'Output: ', output
        input_in = output
    elif opcode == 5:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] <>0:
            alarm[ index] = alarm[ ind1]
            pointerModified = True
            pointerValue = alarm[ ind1]
        else:
            pointerModified = False
    elif opcode == 6:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] ==0:
            alarm[ index] = alarm[ ind1]
            pointerModified = True
            pointerValue = alarm[ ind1]
    elif opcode == 7:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] < alarm[ ind1]:
            alarm[ ind2] = 1
        else:
            alarm[ ind2] = 0
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[ index]
    elif opcode == 8:
        ind0, ind1, ind2 = alarm[index+1]* (not list_op[0]) + (index+1) * list_op[0], alarm[index+2]* (not list_op[1]) + (index+2) * list_op[1], alarm[index+3]* (not list_op[2]) + (index+3) * list_op[2]
        if alarm[ ind0] == alarm[ ind1]:
            alarm[ ind2] = 1
        else:
            alarm[ ind2] = 0
        if ind2 == index:
            pointerModified = True
            pointerValue = alarm[ index]
    elif opcode <> 99:
        wrong = True
        break
    index_sav = index*1
    if opcode in [1,2,99,7,8]:
        index = index+4
    elif opcode in [5,6]:
        index = index+3
    elif opcode in [3,4]:
        index = index+2
    if pointerModified:
        index = pointerValue
  if wrong:
      return np.nan
  else:
      return output

#print opcoding( [3,0,4,0,99], 12,2,1)
#print opcoding( [1002,3,4,3,33], 12,2,1)
#print opcoding( [1101,100,-1,4,0], 12,2,1)
test = copy.deepcopy( alarm)
print opcoding(test, 5)

for test in [ [3,9,8,9,10,9,4,9,99,-1,8], [3,9,7,9,10,9,4,9,99,-1,8], [3,3,1108,-1,8,3,4,3,99], [3,3,1107,-1,8,3,4,3,99]]:
    print "Results", opcoding(test, 5), opcoding(test, 8), opcoding(test, 9)

opcoding( [3,9,8,9,10,9,4,9,99,-1,8], 5)

for test in [ [ 3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]]:
    print "Results", opcoding(test, 0), opcoding(test, 1)

opcoding( [ 3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0)
opcoding( [ 3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 0)

#for test in [ [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]]:
#    print "Results", opcoding(test, 5), opcoding(test, 8), opcoding(test, 9)

