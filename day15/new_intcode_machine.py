
from operator import add, mul, ne, gt, lt, eq
import numpy as np
from copy import deepcopy 
import matplotlib.pyplot as plt

day = 15
do_tests = True
increase_memory = 10000

opcode_ins = { 1: {'oper': add, 'len_code': 3, 'typ_val': 'oper'},
               2: {'oper': mul, 'len_code': 3, 'typ_val': 'oper'},
               3: {             'len_code': 1, 'typ_val': 'Input'},
               4: {             'len_code': 1, 'typ_val': 'Output'},
               5: {'oper': ne,  'len_code': 2, 'typ_val': 'cond_noelse0'},
               6: {'oper': eq,  'len_code': 2, 'typ_val': 'cond_noelse0'},
               7: {'oper': lt,  'len_code': 3, 'typ_val': 'cond_else', 'val': [0,1]},
               8: {'oper': eq,  'len_code': 3, 'typ_val': 'cond_else', 'val': [0,1]},
               9: {'oper': '',  'len_code': 1, 'typ_val': '' },
               99: {'len_code': 0,'typ_val': ''}}

class class_program:
    prog = list()
    index = np.nan
    relat = np.nan
    state = np.nan
    registre = np.nan

def set_registre(self, inputin):
    self.registre = inputin
    return self

def init_prog( prog, inputy=0):
    progy = class_program()
    progy.prog = prog
    # add memory - should be dynamic
    for ii in range(increase_memory):
      progy.prog = progy.prog + [np.nan]
    progy.index = 0
    progy.relat = 0
    progy.registre = inputy
    return progy

def set_param_indexes(mod, i, program):
    if mod == 0:
        param = program.prog[ program.index+i]
    elif mod == 1:
        param = program.index+i
    elif mod == 2:
        param = program.relat + program.prog[ program.index+i]
    return param 

def decode_opcode(program):
    ins = program.prog[ program.index]
    opcode = ins % 100
    modes = [ int(ii) for ii in ("%05d" % ins)[0:-2] ]
    parameters = [set_param_indexes( modes[-ii-1], ii+1, program) for ii in range( opcode_ins[opcode]['len_code'])]
    return opcode, parameters

def intcode_machine( program):
    opcode = -1
    while opcode < 99 and opcode <>4:
        opcode, parameters = decode_opcode(program) 
        #print opcode
        # parameters indices
        # c is always the index where to assign a value
        if opcode_ins[opcode]['len_code'] == 1:
            c =       [ parameters[i] for i in range(1)][0]
        elif opcode_ins[opcode]['len_code'] == 2:
            a, c =    [ parameters[i] for i in range(2)]
        elif opcode_ins[opcode]['len_code'] == 3:
            a, b, c = [ parameters[i] for i in range(3)]
        # compute the operation
        if opcode_ins[opcode]['len_code'] == 3: # 1 2 8 7 
            operation = opcode_ins[opcode]['oper']( program.prog[ a], program.prog[ b])
        if opcode_ins[opcode]['typ_val'] == 'cond_noelse0':
            operation = opcode_ins[opcode]['oper']( program.prog[ a], 0)
        # actuel job
        if opcode_ins[opcode]['typ_val'] == 'oper': # case 1, 2
            program.prog[ c] = operation
        if opcode_ins[opcode]['typ_val'] == 'Input': # case 3
            program.prog[ c] = 1* program.registre
            #program.registre = np.nan # we empty the registre. we assume it is red only once
        if opcode_ins[opcode]['typ_val'] == 'Output': # case 4
            program.registre = 1* program.prog[ c]
        if 'cond_else' == opcode_ins[opcode]['typ_val']: # case 7, 8
            program.prog[ c] = opcode_ins[opcode]['val'][ operation]
        if 'cond_noelse0' == opcode_ins[opcode]['typ_val']: # case 5 6
            if operation: 
                jump = - ( program.index + opcode_ins[opcode]['len_code'] + 1) + program.prog[ c]
            else:
                jump = 0
        else: # case not 5 6
            jump = 0
        if opcode == 9:
            program.relat = program.relat + program.prog[ c]
        program.index = program.index + opcode_ins[opcode]['len_code'] + 1 + jump
        program.state = opcode
        if program.state == 4:
            break
        if program.state == 99:
            break
        #    program.registre = np.nan
    return program

class class_robot():
    oxy = False 
    oxy_coords = np.nan
    coords = (0,0)
    try_step = np.nan

def new_coords( coords, try_step):
    new_coords_out = (coords[0] + direct_update[try_step][0], coords[1] + direct_update[try_step][1])
    return new_coords_out

def update_coords(robot, state):
    if state >= 1:
        robot.coords = new_coords( robot.coords, robot.try_step)
    if state == 2:
        robot.oxy = True
        robot.oxy_coords = robot.coords
    return robot

def new_visit( coords_visited, coords):
    list_2_visit = []
    for directions in direct:
        if not new_coords( coords, directions) in coords_visited:
            list_2_visit.append( directions)
    return list_2_visit

def backtrack( coords_diff, c0, c2):
    if len(c0)>1:
        c1 = c0[-2]
    else:
        c1 = c0[-1]
    for directions in direct_update:
        if direct_update[ directions][0] == c1[0]-c2[0] and direct_update[ directions][1] == c1[1]-c2[1]:
            return directions

def distance( matrice_distance, coords):
    list_dist = []
    for elt in [ matrice_distance[ robot.coords[0] + 25-1, robot.coords[1] + 25], matrice_distance[ robot.coords[0] + 25+1, robot.coords[1] + 25], matrice_distance[ robot.coords[0] + 25, robot.coords[1] + 25+1],  matrice_distance[ robot.coords[0] + 25, robot.coords[1] + 25-1] ]:
        if not np.isnan( elt):
            list_dist.append(elt)
    return min(list_dist) +1

direct = {'n':1, 's': 2, 'w': 3, 'e' :4}
direct_update = {'n': [0,1], 's': [0,-1], 'w': [-1,0], 'e': [1,0]}

##########################################
###  Days computation
##########################################

part2 = False
part2 = True


if day == 15:
    print "Day is:", day
    puzzle = np.genfromtxt('input', dtype='int', delimiter=',').tolist()
    path = [] #, y, color
    coords_2_visit = {} #, y, color
    matrice_distance = np.nan * np.zeros( (50,50)) # too lazy to include this in the coords_2_visit dict
    matrice_distance[  25, 25] = 0
    robot = class_robot()
    path.append( robot.coords) # first coords
    prog_in = init_prog( puzzle, np.nan)
    nb_2visit=1000
    while nb_2visit>0:#robot.oxy == False:
        # if new way - list all direction to explore
        if not robot.coords in coords_2_visit:
                coords_2_visit[ robot.coords] = new_visit( coords_2_visit, robot.coords)
        # choose a way to visit or move back
        if coords_2_visit[ robot.coords] <> []:
            robot.try_step = coords_2_visit[ robot.coords][-1]
            coords_2_visit[ robot.coords].remove( coords_2_visit[ robot.coords][-1])
            # visit it, or try
            prog_in.registre = direct[robot.try_step]
            prog_in = intcode_machine( prog_in)
            # update robot coords
            robot = update_coords(robot, prog_in.registre)
            plt.scatter( robot.coords[0], robot.coords[1])
            if prog_in.registre <> 0:
                path.append( robot.coords)
            # update matrice distance
            matrice_distance[ robot.coords[0] + 25, robot.coords[1] + 25] = distance( matrice_distance, robot.coords)
        else:
            robot.try_step = backtrack( coords_back, path, robot.coords)
            prog_in.registre = direct[robot.try_step]
            prog_in = intcode_machine( prog_in)
            robot.coords = path[-2]
            path = path[0:-1]
        nb_2visit=0
        for still2visit in coords_2_visit:
            if coords_2_visit[ still2visit] <>[]:
                nb_2visit=1
                break
        if part2:
            if robot.oxy == True:
                # reinitiate all our usefull variables, to re-map everything from the oxy
                part2 = False
                path = [] #, y, color
                coords_2_visit = {} #, y, color
                matrice_distance = np.nan * np.zeros( (50,50)) # too lazy to include this in the coords_2_visit dict
                matrice_distance[  robot.oxy_coords[0] + 25, robot.oxy_coords[1] + 25] = 0


dist2ox = matrice_distance[ robot.oxy_coords[0] + 25, robot.oxy_coords[1] + 25]
print dist2ox

# part 2
# the program has a bug for part 2. no need to fix it, the fellowing lines give the answer, 
# as the bug occur when the mapping is finished
wherenan = np.isnan( matrice_distance)
maxdist = np.max( matrice_distance[ ~wherenan])

print maxdist 
